# THIẾT KẾ HỆ THỐNG MẠNG KHÔNG DÂY VÀ BẢO MẬT CHO TRUNG TÂM NGOẠI NGỮ YOLA 
Trung tâm ngoại ngữ Yola cần sử dụng một hệ thống mạng máy tính để phục vụ cho giảng dạy, học tập, quản lý hệ thống mạng cũng như là thuận tiện cho việc chia sẻ tài nguyên thông tin giữa các giảng viên, học viên các Classroom...

Yêu cầu từ khách hàng:
- Hệ thống mạng cần phải được mở rộng băng thông trong mạng nhằm giảm thiểu tắc nghẽn do cùng một lúc có nhiều người truy cập.
- Ngoài ra, yêu cầu về bảo mật cũng là một điều quan trọng đối với trung tâm ngoại ngữ. Có nghĩa là tính bảo mật phải cao nhờ đó giúp mạng nội bộ của trung tâm tránh được sự truy cập trái phép từ bên ngoài. Kiểm soát được luồng thông tin giữa mạng nội bộ, mạng ngoại bộ và mạng Internet, kiểm soát và cấm địa chỉ truy cập đối với các hành vi truy cập không được cấp phép.
- Khả năng kết nối Internet nhanh chóng.
- Tiết kiệm năng lượng trên cơ sở hạ tầng mạng.
- Tất cả thiết bị ở mọi Phòng đều được cấu hình DHCP và dùng xác thực RADIUS. Kết nối được Internet bên ngoài đến Web Server: www.google.com.
- Mô hình sử dụng máy tính của Trung tâm ngoại ngữ Yola như sau:
 + Phòng Classroom 1: gồm có 4 users.
 + Phòng Classroom 2: gồm có 4 users.
 + Phòng Teacher Room: gồm 3 users.
 + Phòng Finance: gồm 4 users.
 + Phòng Reception: gồm 4 users.
 + Phòng HR: gồm 4 users.
 + Phòng Guest: Gồm 1 user.


### Đề xuất hướng giải quyết: 
- 	Xây dựng mô hình mạng chuẩn 3 lớp: Accesss switch, distribution ,core switch.
-	Tất cả các Phòng để được Xác thực bằng RADIUS (Authentication RADIUS).
-	Các thiết bị kết nối mạng ở các Phòng đều được cấu hình IP DHCP
-	Mỗi Phòng sẽ được được thiết kế 1 Access Point riêng. Để phủ sóng cả trung tâm cần ít nhất 4 Access Point.
-	Mỗi 1 giảng viên sẽ được cấp cho một tài khoản và mật khẩu để đăng nhập vào mạng của mỗi phòng để làm việc.
-	Tại trung tâm ngoại ngữ sẽ được thiết kế 1 Multilayer Switch để kết với các Switch bên các Phòng với ADMIN ( được coi là phòng xử lý Kỹ thuật ).
-	Tại Phòng Kỹ thuật được đặt các: DHCP Server, DNS Server, RADIUS Server, và hệ thống mạng được kiểm soát bằng 1 WLC. 
-	Router được đặt tại Phòng Kỹ thuật để kết nối với Internet và mạng cho trung tâm.
Mô hình mạng bao gồm: 1 Router, 4 Server ( DHCP Server, DNS Server, RADIUS Server, Google), 1 Multilayer Switch, 3 Switch, 4 Access Point, 1 WLC và các thiết bị điện tử khác.


### Các ưu điểm của Project:
- Các thiết bị mạng cũng đã được cấu hình trong hệ thống và có thể thực hiện những yêu cầu cần thiết như ping, telnet ,truy cập website bình thường.
- Hệ thống mạng cũng đã đáp ứng được yêu cầu của trung tâm. Trong mạng nội bộ giữa các thiết bị kết nối mạng có thể ping, truyền dữ liệu, trao đổi thông tin với nhau. Hơn nữa, còn có cấu hình WLAN để các mạng Wireless LAN có thể truy cập đến nộ bộ trung tâm.
- Các mạng đều được đảm bảo an toàn và bảo mật thông qua cơ chế xác thực 802.11X và Radius Server. Tại các Switch của trung tâm đều được cấu hìn telnet, để bảo mật, tránh trường hợp kẻ gian xâm nhập được vào hệ thống.

### Định hướng phát triển trong tương lai:
- Trong thời gian sắp tới, trung tâm sẽ triển khai hệ thống mail nội bộ để dễ dàng trao đổi thông tin.
- Có thể tạo thêm kết nối VNP Site-to-Site để giúp tạo đường truyền bảo mật giữa các mạng riêng với nhau.
- Hơn nữa để hệ thống mạng an toàn thì việc phát triển đầu tiên phải là nâng cấp tính bảo mật của mô hình mạng, để tránh những kẻ gian có thể xâm nhập vào hệ thống dễ dàng.

